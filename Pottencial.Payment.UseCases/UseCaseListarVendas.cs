﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Borders.UseCases;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;

namespace Pottencial.Payment.UseCases
{
    public class UseCaseListarVendas : IUseCaseListarVendas
    {
        private readonly ILogger<UseCaseListarVendas> _logger;
        private readonly IMapper _mapper;
        private readonly IVendasRepository _vendasRepository;
        private readonly IVendedorRepository _vendedorRepository;
        private readonly IItemRepository _itemRepository;

        public UseCaseListarVendas(IMapper mapper,
                                    IVendasRepository vendasRepository,
                                    IVendedorRepository vendedorRepository,
                                    IItemRepository itemRepository,
                                    ILogger<UseCaseListarVendas> logger)
        {
            _mapper = mapper;
            _vendasRepository = vendasRepository;
            _vendedorRepository = vendedorRepository;
            _itemRepository = itemRepository;
            _logger = logger;
        }

        public async Task<List<VendaResponse>> Execute()
        {
            try
            {
                var vendas = await _vendasRepository.GetAllSale();

                if (vendas == null)
                    throw new VendaNaoEncontradaException($"Venda não encontrada!");

                var listaResponse = new List<VendaResponse>(); 

                foreach (var venda in vendas)
                {
                    var vendedor = await _vendedorRepository.GetByIdSeller(venda.IdVendedor);
                    if (vendedor == null)
                        throw new Exception($"Vendedor não encontrada! {venda.IdVendedor}");

                    var itens = await _itemRepository.GetItemByIdSale(venda.Id);
                    if (itens == null)
                        throw new Exception($"Itens da venda não encontrado!");


                    var response = _mapper.Map<VendaResponse>(venda);

                    response.Vendedor = _mapper.Map<VendedorDTO>(vendedor);

                    response.ListaDeItens = itens.ConvertAll(x => new ItemDTO(x.Nome) { });

                    listaResponse.Add(response);
                }

                return listaResponse;
            }
            catch (VendaNaoEncontradaException ex)
            {
                _logger.LogError($"Erro: " + ex.Message);
                
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERRO: {ex.Message}");
            }

            return null;    
        }

        
    }
}
