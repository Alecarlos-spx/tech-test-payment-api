﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Borders.UseCases;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enum;
using Pottencial.Payment.Domain.Exceptions;

namespace Pottencial.Payment.UseCases
{
    public class UseCaseRegistrarVenda : IUseCaseResgistrarVenda
    {
        private readonly ILogger<UseCaseRegistrarVenda> _logger;
        private readonly IMapper _mapper;
        private readonly IVendasRepository _vendasRepository;
        private readonly IVendedorRepository _vendedorRepository;
        private readonly IItemRepository _itemRepository;

        public UseCaseRegistrarVenda(IVendasRepository vendasRepository,
            IMapper mapper,
            IVendedorRepository vendedorRepository,
            IItemRepository itemRepository,
            ILogger<UseCaseRegistrarVenda> logger)
        {
            _vendasRepository = vendasRepository;
            _mapper = mapper;
            _vendedorRepository = vendedorRepository;
            _itemRepository = itemRepository;
            _logger = logger;
        }

        public async Task<VendaResponse> Execute(RegistrarVendaRequest request)
        {
            try
            {
                if (request == null)
                    return null;


                var vendedor = _mapper.Map<Vendedor>(request.Vendedor);

                await _vendedorRepository.AddSeller(vendedor);

                var venda = new Venda(
                    idVendedor: vendedor.Id,
                    dataDaVenda: request.DataDaVenda.Date,
                    statusDaVenda: EnumStatusDaVenda.AguardandoPagamento
                );

                await _vendasRepository.AddSale(venda);

                var response = _mapper.Map<VendaResponse>(venda);

                var listaItens = new List<ItemDTO>();

                foreach (var itemVenda in request.ListaDeItens)
                {
                    var item = new Item(
                        nome: itemVenda.Nome,
                        vendaId: venda.Id
                    );

                    await _itemRepository.AddItem(item);

                    listaItens.Add(new ItemDTO(item.Nome));

                }
                response.Vendedor = _mapper.Map<VendedorDTO>(vendedor);
                response.ListaDeItens = new List<ItemDTO>();
                response.ListaDeItens.AddRange(listaItens);

                return response;
            }
            catch (Exception ex)
            {

                _logger.LogError($"ERRO: {ex.Message}");
            }

            return null;

        }
    }
}
