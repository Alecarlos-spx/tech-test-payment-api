﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Borders.UseCases;
using Pottencial.Payment.Domain.Exceptions;

namespace Pottencial.Payment.UseCases
{
    public class UseCaseAtualizarStatusVenda : IUseCaseAtualizarStatusVenda
    {
        private readonly ILogger<UseCaseAtualizarStatusVenda> _logger;
        private readonly IMapper _mapper;
        private readonly IVendasRepository _vendasRepository;
        private readonly IVendedorRepository _vendedorRepository;
        private readonly IItemRepository _itemRepository;

        public UseCaseAtualizarStatusVenda(IMapper mapper,
                                            IVendasRepository vendasRepository,
                                            IVendedorRepository vendedorRepository,
                                            IItemRepository itemRepository,
                                            ILogger<UseCaseAtualizarStatusVenda> logger)
        {
            _mapper = mapper;
            _vendasRepository = vendasRepository;
            _vendedorRepository = vendedorRepository;
            _itemRepository = itemRepository;
            _logger = logger;
        }

        public async Task<VendaResponse> Execute(Guid id, AtualizarStatusVendaRequest request)
        {
            
            try
            {
                var venda = await _vendasRepository.GetByIdSale(id);

                venda.AtualizarStatus(request.StatusVenda);

                if (!await _vendasRepository.UpdateStatus(venda))
                    return null;

                var vendedor = await _vendedorRepository.GetByIdSeller(venda.IdVendedor);

                var itens = await _itemRepository.GetItemByIdSale(id);

                var response = _mapper.Map<VendaResponse>(venda);

                response.Vendedor = _mapper.Map<VendedorDTO>(vendedor);

                response.ListaDeItens = itens.ConvertAll(x => new ItemDTO(x.Nome) { });

                return response;
            }
            catch (VendaNaoEncontradaException ex)
            {
                _logger.LogError($"Erro: " + ex.Message);
                
            }
            catch (AtualizacaoDeStatusIncorretaException ex)
            {
                _logger.LogError($"ERRO: Atualização de Status da Venda não Permitida!");
                
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERRO: {ex.Message}");
            }

            return null;
        }
    }
}
