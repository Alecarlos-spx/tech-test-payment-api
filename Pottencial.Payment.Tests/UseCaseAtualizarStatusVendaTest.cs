﻿using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enum;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Repositories;
using Pottencial.Payment.Tests.Builder;
using Pottencial.Payment.UseCases;

namespace Pottencial.Payment.Tests
{
    public class UseCaseAtualizarStatusVendaTest
    {
        private readonly ILogger<UseCaseAtualizarStatusVenda> _logger;
        private readonly UseCaseAtualizarStatusVenda useCase;

        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IVendasRepository> _vendasRepository;
        private readonly Mock<IVendedorRepository> _vendedorRepository;
        private readonly Mock<IItemRepository> _itemRepository;

        public UseCaseAtualizarStatusVendaTest()
        {
            _logger = new Mock<ILogger<UseCaseAtualizarStatusVenda>>().Object;

            _mapper = new Mock<IMapper>();
            _vendasRepository = new Mock<IVendasRepository>();
            _vendedorRepository = new Mock<IVendedorRepository>();
            _itemRepository = new Mock<IItemRepository>();
            useCase = new UseCaseAtualizarStatusVenda(
                            _mapper.Object,
                            _vendasRepository.Object,
                            _vendedorRepository.Object,
                            _itemRepository.Object,
                            _logger);
        }

        [Fact]
        public async Task UseCase_AtualizarStatusStatusVendaOk()
        {
            #region arrange

            var id = Guid.NewGuid();

            var request = new AtualizarStatusVendaRequest() { StatusVenda = EnumStatusDaVenda.PagamentoAprovado };

            var venda = new VendaBuilder().Build();
            var vendedor = new VendedorBuilder().Build();
            var itens = new List<Item>() { new ItemBuilder().Build() };
            var vendaResponse = new VendaResponseBuilder().Build();
            var vendedorDTO = new VendedorDTOBuilder().Build();

            _vendasRepository.Setup(it => it.GetByIdSale(It.IsAny<Guid>())).ReturnsAsync(venda);
            
            //venda.AtualizarStatus(EnumStatusDaVenda.PagamentoAprovado);

            _vendasRepository.Setup(it => it.UpdateStatus(It.IsAny<Venda>())).ReturnsAsync(true);

            _vendedorRepository.Setup(it => it.GetByIdSeller(It.IsAny<Guid>())).ReturnsAsync(vendedor);
            _itemRepository.Setup(it => it.GetItemByIdSale(It.IsAny<Guid>())).ReturnsAsync(itens);

            _mapper.Setup(it => it.Map<VendaResponse>(It.IsAny<Venda>())).Returns(vendaResponse);

            _mapper.Setup(it => it.Map<VendedorDTO>(It.IsAny<Vendedor>())).Returns(vendedorDTO);

            var listaResponse = new List<VendaResponse>();
            listaResponse.Add(vendaResponse);

            #endregion

            #region act
            var response = await useCase.Execute(id, request);
            #endregion

            #region assert
            response.Should().NotBeNull();
            _vendasRepository.Verify(it => it.UpdateStatus(It.IsAny<Venda>()), Times.Once);

            #endregion
        }

        [Theory]
        [InlineData(EnumStatusDaVenda.AguardandoPagamento, EnumStatusDaVenda.EnviadoParaTransportadora)]
        [InlineData(EnumStatusDaVenda.AguardandoPagamento, EnumStatusDaVenda.Entregue)]
        [InlineData(EnumStatusDaVenda.PagamentoAprovado, EnumStatusDaVenda.AguardandoPagamento)]
        [InlineData(EnumStatusDaVenda.PagamentoAprovado, EnumStatusDaVenda.Entregue)]
        [InlineData(EnumStatusDaVenda.EnviadoParaTransportadora, EnumStatusDaVenda.AguardandoPagamento)]
        [InlineData(EnumStatusDaVenda.EnviadoParaTransportadora, EnumStatusDaVenda.PagamentoAprovado)]
        [InlineData(EnumStatusDaVenda.EnviadoParaTransportadora, EnumStatusDaVenda.Cancelada)]
        [InlineData(EnumStatusDaVenda.Cancelada, EnumStatusDaVenda.AguardandoPagamento)]
        [InlineData(EnumStatusDaVenda.Cancelada, EnumStatusDaVenda.PagamentoAprovado)]
        [InlineData(EnumStatusDaVenda.Cancelada, EnumStatusDaVenda.EnviadoParaTransportadora)]
        [InlineData(EnumStatusDaVenda.Cancelada, EnumStatusDaVenda.Entregue)]
        public async Task UseCase_AtualizarStatusStatusVenda_RetornoFalha(EnumStatusDaVenda statusAtual, EnumStatusDaVenda novoStatus)
        {
            #region arrange

            var id = Guid.NewGuid();

            var request = new AtualizarStatusVendaRequest() { StatusVenda = novoStatus };

            var venda = new VendaBuilder().WithStatus(statusAtual).Build();
            var vendedor = new VendedorBuilder().Build();
            var itens = new List<Item>() { new ItemBuilder().Build() };
            var vendaResponse = new VendaResponseBuilder().Build();
            var vendedorDTO = new VendedorDTOBuilder().Build();

            _vendasRepository.Setup(it => it.GetByIdSale(It.IsAny<Guid>())).ReturnsAsync(venda);
            //venda.AtualizarStatus(novoStatus);

            _vendasRepository.Setup(it => it.UpdateStatus(It.IsAny<Venda>())).ReturnsAsync(true);

            _vendedorRepository.Setup(it => it.GetByIdSeller(It.IsAny<Guid>())).ReturnsAsync(vendedor);
            _itemRepository.Setup(it => it.GetItemByIdSale(It.IsAny<Guid>())).ReturnsAsync(itens);

            _mapper.Setup(it => it.Map<VendaResponse>(It.IsAny<Venda>())).Returns(vendaResponse);

            _mapper.Setup(it => it.Map<VendedorDTO>(It.IsAny<Vendedor>())).Returns(vendedorDTO);

            var listaResponse = new List<VendaResponse>();
            listaResponse.Add(vendaResponse);

            #endregion

            #region act
            var response = await useCase.Execute(id, request);
            #endregion

            #region assert

            Assert.Throws<AtualizacaoDeStatusIncorretaException>(() => venda.AtualizarStatus(novoStatus));
            #endregion
        }
    }
}
