﻿using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Payment.Tests.Builder
{
    class ItemDTOBuilder
    {
        private readonly ItemDTO instance;
        public ItemDTOBuilder()
        {
            instance = new ItemDTO()
            {
                Nome = "Sulfite"
            };
        }

        public ItemDTO Build() => instance;
    }
}
