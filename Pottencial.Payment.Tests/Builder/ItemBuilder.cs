﻿using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Tests.Builder
{
    class ItemBuilder
    {
        private readonly Item instance;
        public ItemBuilder()
        {
            instance = new Item()
            {
                Nome = "Sulfite",
                VendaId = Guid.NewGuid(),
            };
        }

        public Item Build() => instance;
    }
}
