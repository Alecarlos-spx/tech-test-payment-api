﻿using Pottencial.Payment.Borders.Dtos;

namespace Pottencial.Payment.Tests.Builder
{
    class VendedorDTOBuilder
    {
        public readonly VendedorDTO instance;

        public VendedorDTOBuilder()
        {
            instance = new VendedorDTO()
            {
                Cpf = "123456789-2",
                Nome = "Vendedor",
                Email = "Email@teste.com.br",
                Telefone = "11975566553"
            };
        }

        public VendedorDTO Build() => instance;
    }
}
