﻿using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enum;

namespace Pottencial.Payment.Tests.Builder
{
    public class VendaBuilder
    {
        private readonly Venda instance;
        public VendaBuilder()
        {
            instance = new Venda()
            {
                DataDaVenda = DateTime.Now,
                IdVendedor = Guid.NewGuid(),
                StatusDaVenda = EnumStatusDaVenda.AguardandoPagamento,
                Vendedor = new VendedorBuilder().Build(),
                Itens = new List<Item>()
                { 
                    new ItemBuilder().Build()
                }
            };
        }

        public Venda Build() => instance;

        public VendaBuilder WithStatus(EnumStatusDaVenda status)
        {
            instance.StatusDaVenda = status;
            return this;
        }
    }
}
