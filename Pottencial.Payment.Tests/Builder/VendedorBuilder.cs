﻿using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Tests.Builder
{
    class VendedorBuilder
    {
        public readonly Vendedor instance;

        public VendedorBuilder()
        {
            instance = new Vendedor()
            {
                Cpf = "123456789-2",
                Nome = "Vendedor",
                Email = "Email@teste.com.br",
                Telefone = "11975566553"
            };
        }

        public Vendedor Build() => instance;
    }
}
