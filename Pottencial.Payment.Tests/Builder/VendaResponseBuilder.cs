﻿using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Domain.Enum;

namespace Pottencial.Payment.Tests.Builder
{
    public class VendaResponseBuilder
    {
        private readonly VendaResponse instance;
        public VendaResponseBuilder()
        {
            instance = new VendaResponse()
            {
                DataDaVenda = DateTime.Now,
                Id = Guid.NewGuid(),
                StatusDaVenda = EnumStatusDaVenda.AguardandoPagamento,
                Vendedor = new VendedorDTOBuilder().Build(),
                ListaDeItens = new List<ItemDTO>() { new ItemDTOBuilder().Build() }
            };

        }

        public VendaResponse Build() => instance;
    }
}
