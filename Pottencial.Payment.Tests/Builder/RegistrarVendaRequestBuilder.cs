﻿using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Payment.Tests.Builder
{
    public class RegistrarVendaRequestBuilder
    {
        private readonly RegistrarVendaRequest instance;
        public RegistrarVendaRequestBuilder()
        {
            instance = new RegistrarVendaRequest()
            {
                DataDaVenda = DateTime.Now,

                Vendedor = new VendedorDTOBuilder().Build(),
                ListaDeItens =
                new List<ItemDTO>()
                {
                    new ItemDTOBuilder().Build()
                }
            };
        }

        public RegistrarVendaRequest Build() => instance;

    }
}
