﻿using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Tests.Builder;
using Pottencial.Payment.UseCases;

namespace Pottencial.Payment.Tests
{
    public class UseCaseRegistrarVendaTest
    {
        private readonly ILogger<UseCaseRegistrarVenda> _logger;
        private readonly UseCaseRegistrarVenda useCase;

        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IVendasRepository> _vendasRepository;
        private readonly Mock<IVendedorRepository> _vendedorRepository;
        private readonly Mock<IItemRepository> _itemRepository;

        public UseCaseRegistrarVendaTest()
        {
            _logger = new Mock<ILogger<UseCaseRegistrarVenda>>().Object;

            _mapper = new Mock<IMapper>();
            _vendasRepository = new Mock<IVendasRepository>();
            _vendedorRepository = new Mock<IVendedorRepository>();
            _itemRepository = new Mock<IItemRepository>();
            useCase = new UseCaseRegistrarVenda(
                            _vendasRepository.Object,
                            _mapper.Object,
                            _vendedorRepository.Object,
                            _itemRepository.Object,
                            _logger);
        }

        [Fact]
        public async Task UseCase_ReturnsOk()
        {
            #region arrange
            var registrarVendaRequest = new RegistrarVendaRequestBuilder().Build();
            var venda = new VendaBuilder().Build();
            var vendedor = new VendedorBuilder().Build();
            var item = new ItemBuilder().Build();
            var vendaResponse = new VendaResponseBuilder().Build();
            var vendedorDTO = new VendedorDTOBuilder().Build();

            _mapper.Setup(it => it.Map<Vendedor>(It.IsAny<VendedorDTO>())).Returns(vendedor);

            _vendedorRepository.Setup(it => it.AddSeller(It.IsAny<Vendedor>())).ReturnsAsync(vendedor);

            _vendasRepository.Setup(it => it.AddSale(It.IsAny<Venda>())).ReturnsAsync(venda);
            _itemRepository.Setup(it => it.AddItem(It.IsAny<Item>())).ReturnsAsync(item);

            _mapper.Setup(it => it.Map<VendedorDTO>(It.IsAny<Vendedor>())).Returns(vendedorDTO);
            _mapper.Setup(it => it.Map<VendaResponse>(It.IsAny<Venda>())).Returns(vendaResponse);

            

            var listaResponse = new List<VendaResponse>();
            listaResponse.Add(vendaResponse);

            #endregion

            #region act
            var response = await useCase.Execute(registrarVendaRequest);
            #endregion

            #region assert
            response.Should().NotBeNull();
            _vendasRepository.Verify(it => it.AddSale(It.IsAny<Venda>()), Times.Once);
            _vendedorRepository.Verify(it => it.AddSeller(It.IsAny<Vendedor>()), Times.Once);
            _itemRepository.Verify(it => it.AddItem(It.IsAny<Item>()), Times.Once);
            #endregion
        }

        [Fact]
        public async Task UseCase_ErroRegistrarVenda()
        {
            #region arrange
            RegistrarVendaRequest registrarVendaRequest = null;
            Venda venda = null;
            var vendedor = new VendedorBuilder().Build();
            var item = new ItemBuilder().Build();
            var vendaResponse = new VendaResponseBuilder().Build();
            var vendedorDTO = new VendedorDTOBuilder().Build();

            _mapper.Setup(it => it.Map<Vendedor>(It.IsAny<VendedorDTO>())).Returns(vendedor);

            _vendedorRepository.Setup(it => it.AddSeller(It.IsAny<Vendedor>())).ReturnsAsync(vendedor);

            _vendasRepository.Setup(it => it.AddSale(It.IsAny<Venda>())).ReturnsAsync(venda);
            _itemRepository.Setup(it => it.AddItem(It.IsAny<Item>())).ReturnsAsync(item);

            _mapper.Setup(it => it.Map<VendedorDTO>(It.IsAny<Vendedor>())).Returns(vendedorDTO);
            _mapper.Setup(it => it.Map<VendaResponse>(It.IsAny<Venda>())).Returns(vendaResponse);



            var listaResponse = new List<VendaResponse>();
            listaResponse.Add(vendaResponse);

            #endregion

            #region act
            var response = await useCase.Execute(registrarVendaRequest);
            #endregion

            #region assert
            response.Should().BeNull();
            #endregion
        }
    }
}
