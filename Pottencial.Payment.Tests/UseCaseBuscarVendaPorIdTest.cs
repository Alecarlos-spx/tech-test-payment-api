﻿using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Tests.Builder;
using Pottencial.Payment.UseCases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Payment.Tests
{
    public class UseCaseBuscarVendaPorIdTest
    {
        private readonly ILogger<UseCaseBuscarVendaPorId> _logger;
        private readonly UseCaseBuscarVendaPorId useCase;

        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IVendasRepository> _vendasRepository;
        private readonly Mock<IVendedorRepository> _vendedorRepository;
        private readonly Mock<IItemRepository> _itemRepository;

        public UseCaseBuscarVendaPorIdTest()
        {
            _logger = new Mock<ILogger<UseCaseBuscarVendaPorId>>().Object;

            _mapper = new Mock<IMapper>();
            _vendasRepository = new Mock<IVendasRepository>();
            _vendedorRepository = new Mock<IVendedorRepository>();
            _itemRepository = new Mock<IItemRepository>();
            useCase = new UseCaseBuscarVendaPorId(
                            _mapper.Object,
                            _vendasRepository.Object,
                            _vendedorRepository.Object,
                            _itemRepository.Object,
                            _logger);
        }

        [Fact]
        public async Task UseCase_ReturnsOk()
        {
            #region arrange
            var id = Guid.NewGuid();

            var vendas = new VendaBuilder().Build();
            var vendedor = new VendedorBuilder().Build();
            var itens = new List<Item>() { new ItemBuilder().Build() };
            var vendaResponse = new VendaResponseBuilder().Build();
            var vendedorDTO = new VendedorDTOBuilder().Build();

            _vendasRepository.Setup(it => it.GetByIdSale(It.IsAny<Guid>())).ReturnsAsync(vendas);
            _vendedorRepository.Setup(it => it.GetByIdSeller(It.IsAny<Guid>())).ReturnsAsync(vendedor);
            _itemRepository.Setup(it => it.GetItemByIdSale(It.IsAny<Guid>())).ReturnsAsync(itens);

            _mapper.Setup(it => it.Map<VendaResponse>(It.IsAny<Venda>())).Returns(vendaResponse);

            _mapper.Setup(it => it.Map<VendedorDTO>(It.IsAny<Vendedor>())).Returns(vendedorDTO);

            var listaResponse = new List<VendaResponse>();
            listaResponse.Add(vendaResponse);

            #endregion

            #region act
            var response = await useCase.Execute(id);
            #endregion

            #region assert
            response.Should().NotBeNull();
            _vendasRepository.Verify(it => it.GetByIdSale(It.IsAny<Guid>()), Times.Once);
            _vendedorRepository.Verify(it => it.GetByIdSeller(It.IsAny<Guid>()), Times.Once);
            _itemRepository.Verify(it => it.GetItemByIdSale(It.IsAny<Guid>()), Times.Once);
            #endregion
        }


        [Fact]
        public async Task UseCase_VendaNaoEncontradaOk()
        {
            #region arrange
            Guid id = Guid.Empty;

            var venda = new VendaBuilder().Build();
            var vendedor = new VendedorBuilder().Build();
            var itens = new List<Item>() { new ItemBuilder().Build() };
            var vendaResponse = new VendaResponseBuilder().Build();
            var vendedorDTO = new VendedorDTOBuilder().Build();


            _vendasRepository.Setup(it => it.GetByIdSale(It.IsAny<Guid>())).ReturnsAsync(venda); 
            #endregion

            #region act

            var response = await useCase.Execute(id);
            #endregion

            #region assert
            
            response.Should().BeNull();
            _vendasRepository.Verify(it => it.GetByIdSale(It.IsAny<Guid>()), Times.Once);
            #endregion
        }
    }
}
