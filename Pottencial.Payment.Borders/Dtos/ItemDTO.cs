﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Borders.Dtos
{
    public class ItemDTO
    {
        public ItemDTO(string nome)
        {
            Nome = nome;
        }

        public ItemDTO()
        {
        }

        [Required(ErrorMessage = "Por Favor, insira o item da venda!")]
        public string Nome { get; set; }
    }
}
