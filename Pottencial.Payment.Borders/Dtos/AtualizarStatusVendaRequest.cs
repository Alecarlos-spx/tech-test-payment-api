﻿using Pottencial.Payment.Domain.Enum;
using System.Text.Json.Serialization;

namespace Pottencial.Payment.Borders.Dtos
{
    public class AtualizarStatusVendaRequest
    {
        [JsonIgnore]
        public Guid Id { get; set; }
        public EnumStatusDaVenda StatusVenda { get; set; }
    }
}
