﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Borders.Dtos
{
    public class RegistrarVendaRequest
    {
        public DateTime DataDaVenda { get; set; }

        [Required(ErrorMessage = "Por Favor, insira o vendedor!")]
        public VendedorDTO Vendedor { get; set; }

        [Required(ErrorMessage = "Por Favor, insira a lista de itens!")]
        public List<ItemDTO> ListaDeItens { get; set; }
    }
}
