﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Borders.Dtos
{
    public class VendedorDTO
    {
        [Required(ErrorMessage = "Por Favor, insira o CPF!")]
        public string Cpf { get; set; }

        [Required(ErrorMessage = "Por Favor, insira o nome!")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Por Favor, insira o E-mail!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Por Favor, insira o telefone!")]
        public string Telefone { get; set; }
    }
}
