﻿using Pottencial.Payment.Domain.Enum;

namespace Pottencial.Payment.Borders.Dtos
{
    public class VendaResponse
    {
        public Guid Id { get; set; }
        public DateTime DataDaVenda { get; set; }
        public EnumStatusDaVenda StatusDaVenda { get; set; }

        public VendedorDTO Vendedor { get; set; }

        public List<ItemDTO> ListaDeItens { get; set; }
    }
}
