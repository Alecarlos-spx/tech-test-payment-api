﻿using Pottencial.Payment.Borders.Dtos;

namespace Pottencial.Payment.Borders.UseCases
{
    public interface IUseCaseAtualizarStatusVenda
    {
        Task<VendaResponse> Execute(Guid id, AtualizarStatusVendaRequest request);
    }
}
