﻿using Pottencial.Payment.Borders.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Payment.Borders.UseCases
{
    public interface IUseCaseListarVendas
    {
        Task<List<VendaResponse>> Execute();
    }
}
