﻿using Pottencial.Payment.Borders.Dtos;

namespace Pottencial.Payment.Borders.UseCases
{
    public interface IUseCaseResgistrarVenda
    {
       Task<VendaResponse> Execute(RegistrarVendaRequest request);
    }
}
