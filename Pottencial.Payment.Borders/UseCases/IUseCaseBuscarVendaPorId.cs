﻿using Pottencial.Payment.Borders.Dtos;

namespace Pottencial.Payment.Borders.UseCases
{
    public interface IUseCaseBuscarVendaPorId
    {
        Task<VendaResponse> Execute(Guid id);
    }
}
