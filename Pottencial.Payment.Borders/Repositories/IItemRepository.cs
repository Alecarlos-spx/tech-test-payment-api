﻿using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Borders.Repositories
{
    public interface IItemRepository
    {
        Task<Item> AddItem(Item request);
        Task<Item> GetByIdItem(Guid id);
        Task<List<Item>> GetItemByIdSale(Guid id);
    }
}
