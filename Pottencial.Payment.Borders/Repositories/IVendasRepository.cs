﻿using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Borders.Repositories
{
    public interface IVendasRepository
    {
        Task<Venda> AddSale(Venda request);
        Task<Venda> GetByIdSale(Guid id);
        Task<bool> UpdateStatus(Venda request);
        Task<List<Venda>> GetAllSale();

    }
}
