﻿namespace Pottencial.Payment.Borders.Repositories
{
    public interface IUnitOfWork
    {
        Task<bool> CommitAsync();
    }
}
