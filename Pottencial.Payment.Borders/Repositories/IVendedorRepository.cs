﻿using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Borders.Repositories
{
    public interface IVendedorRepository
    {
        Task<Vendedor> AddSeller(Vendedor request);
        Task<Vendedor> GetByIdSeller(Guid id);
    }
}
