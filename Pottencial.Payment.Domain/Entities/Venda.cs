﻿using Pottencial.Payment.Domain.Enum;
using Pottencial.Payment.Domain.Exceptions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pottencial.Payment.Domain.Entities
{
    public class Venda
    {
        [Key]
        public Guid Id { get; set; }
        public Guid IdVendedor { get; set; }

        [ForeignKey("IdVendedor")]
        public Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }
        public DateTime DataDaVenda { get; set; }
        public EnumStatusDaVenda StatusDaVenda { get; set; }

        public Venda()
        {
            Id = Guid.NewGuid();
        }

        public Venda(Guid idVendedor, DateTime dataDaVenda, EnumStatusDaVenda statusDaVenda)
        {
            Id = Guid.NewGuid();
            IdVendedor = idVendedor;
            DataDaVenda = dataDaVenda;
            StatusDaVenda = statusDaVenda;
        }

        public Venda(Guid id, Guid idVendedor, DateTime dataDaVenda, EnumStatusDaVenda statusDaVenda)
        {
            Id = id;
            IdVendedor = idVendedor;
            DataDaVenda = dataDaVenda;
            StatusDaVenda = statusDaVenda;
        }

        public Venda(Guid id, Guid idVendedor, 
                DateTime dataDaVenda, EnumStatusDaVenda statusDaVenda,
                Vendedor vendedor, List<Item> itens)
        {
            Id = id;
            IdVendedor = idVendedor;
            DataDaVenda = dataDaVenda;
            StatusDaVenda = statusDaVenda;
            Vendedor = vendedor;
            Itens = itens; 
        }

        public Venda(Vendedor vendedor, List<Item> itens, DateTime dataDaVenda)
        {
            Id = Guid.NewGuid();

            Vendedor = vendedor ?? throw new VendedorException("Por favor, informe o vendedor!");
            if (itens == null || itens.Count == 0)
            {
                throw new ItensDaVendaException("Por favor, informe os itens da venda!");
            }
            else
            {
                Itens = itens;
            }

            if (dataDaVenda == DateTime.MinValue)
            {
                throw new DataDaVendaException("Por favor, informe a data!");
            }
            else
            {
                DataDaVenda = dataDaVenda;
            }

            StatusDaVenda = EnumStatusDaVenda.AguardandoPagamento;
        }

       
        public void AtualizarStatus(EnumStatusDaVenda novoStatus)
        {
            if ((StatusDaVenda == EnumStatusDaVenda.AguardandoPagamento) &&
                (novoStatus == EnumStatusDaVenda.PagamentoAprovado ||
                 novoStatus == EnumStatusDaVenda.Cancelada))
            {
                StatusDaVenda = novoStatus;
            }
            else if ((StatusDaVenda == EnumStatusDaVenda.PagamentoAprovado) &&
                (novoStatus == EnumStatusDaVenda.EnviadoParaTransportadora ||
                 novoStatus == EnumStatusDaVenda.Cancelada))
            {
                StatusDaVenda = novoStatus;
            }
            else if ((StatusDaVenda == EnumStatusDaVenda.EnviadoParaTransportadora) &&
                 (novoStatus == EnumStatusDaVenda.Entregue))
            {
                StatusDaVenda = novoStatus;
            }
            else
            {
                throw new AtualizacaoDeStatusIncorretaException(
                    $"Atualização de Status Incorreta! Por Favor, verifique e realize a correção - " +
                    $"{StatusDaVenda} para {novoStatus}");
            }
        }
    }
}
