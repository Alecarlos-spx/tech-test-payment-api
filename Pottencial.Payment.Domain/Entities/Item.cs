﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pottencial.Payment.Domain.Entities
{
    public class Item
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public Guid VendaId { get; set; }

        [ForeignKey("VendaId")]
        public Venda Venda { get; set; }

        public Item()
        {
            Id = Guid.NewGuid();
        }

        public Item(string nome, Guid vendaId)
        {
            Id = Guid.NewGuid();

            Nome = nome;

            VendaId = vendaId;
        }

        public Item(Guid id, string nome, Guid vendaId)
        {
            Id = id;
            Nome = nome;
            VendaId = vendaId;
        }

        public Item(string nome)
        {
            Nome = nome;
        }
    }
}
