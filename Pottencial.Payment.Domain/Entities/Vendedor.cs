﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Domain.Entities
{
    public class Vendedor
    {
 
        public Guid Id { get; set; }
        [Required]
        public string Cpf { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Telefone { get; set; }

        public Vendedor()
        {
            Id = Guid.NewGuid();
        }

        public Vendedor(string cpf, string nome, string email, string telefone)
        {
            Id = Guid.NewGuid();

            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }

        public Vendedor(Guid id, string cpf, string nome, string email, string telefone)
        {
            Id = id;
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }
    }
}
