﻿
namespace Pottencial.Payment.Domain.Exceptions
{
    public class VendedorException : Exception
    {
        public VendedorException(string message) : base(message) { }
    }
}
