﻿namespace Pottencial.Payment.Domain.Exceptions
{
    public class DataDaVendaException : Exception
    {
        public DataDaVendaException(string message) : base(message) { }
    }
}
