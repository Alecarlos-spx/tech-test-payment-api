﻿namespace Pottencial.Payment.Domain.Exceptions
{
    public class ItensDaVendaException : Exception
    {
        public ItensDaVendaException(string message) : base(message) { }
    }
}
