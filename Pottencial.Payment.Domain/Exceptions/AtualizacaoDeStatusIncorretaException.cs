﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Payment.Domain.Exceptions
{
    public class AtualizacaoDeStatusIncorretaException : Exception
    {
        public AtualizacaoDeStatusIncorretaException(string message) : base(message) { }
    }
}
