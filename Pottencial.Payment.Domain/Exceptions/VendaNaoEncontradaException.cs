﻿namespace Pottencial.Payment.Domain.Exceptions
{
    public class VendaNaoEncontradaException : Exception
    {
        public VendaNaoEncontradaException(string message) : base(message)
        {
        }
    }
}
