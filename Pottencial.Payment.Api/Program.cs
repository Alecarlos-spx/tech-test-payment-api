using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Api.Configurations;
using Pottencial.Payment.Borders.Filter;
using Pottencial.Paymento.Infrastructure.Context;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
//Banco de Dados InMemory
builder.Services.AddDbContext<SalesContext>(opt => opt.UseInMemoryDatabase("PottencialPayment"));

/*builder.Services.AddDbContext<SalesContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("ConexaoPadrao")));
*/


builder.Services.AddControllers(options =>
    options.Filters.Add(typeof(ErrorResponseFilter))
    ).AddJsonOptions(options =>
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddAutoMapperApi(typeof(MapperProfile));
builder.Services.AddRepositories();
builder.Services.AddUseCases();



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c => { c.RouteTemplate = "api-docs/{documentName}/open-api.json"; });
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/api-docs/v1/open-api.json", "Pottencial.Payment.Api v1");
        c.RoutePrefix = "api-docs";
        c.OAuthScopeSeparator(" ");
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
