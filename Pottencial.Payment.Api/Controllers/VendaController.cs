﻿using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Borders.UseCases;
using Pottencial.Payment.Domain.Entities;
using System.Net;

namespace Pottencial.Payment.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class VendaController : ControllerBase
    {
        private readonly ILogger<VendaController> _logger;
        private readonly IUseCaseResgistrarVenda _useCaseResgistrarVenda;
        private readonly IUseCaseBuscarVendaPorId _useCaseBuscarVendaPorId;
        private readonly IUseCaseListarVendas _useCaseListarVendas;
        private readonly IUseCaseAtualizarStatusVenda _useCaseAtualizarStatusVenda;

        public VendaController(ILogger<VendaController> logger,
                IUseCaseResgistrarVenda useCaseResgistrarVenda,
                IUseCaseBuscarVendaPorId useCaseBuscarVendaPorId,
                IUseCaseListarVendas useCaseListarVendas,
                IUseCaseAtualizarStatusVenda useCaseAtualizarStatusVenda)
        {
            _logger = logger;
            _useCaseResgistrarVenda = useCaseResgistrarVenda;
            _useCaseBuscarVendaPorId = useCaseBuscarVendaPorId;
            _useCaseListarVendas = useCaseListarVendas;
            _useCaseAtualizarStatusVenda = useCaseAtualizarStatusVenda;
        }
        /// <summary>
        /// Resgistro de Venda com itens
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType((int)HttpStatusCode.OK,                   Type = typeof(VendaResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest,           Type = typeof(ErrorResponse))]
        [ProducesResponseType((int)HttpStatusCode.NotFound,             Type = typeof(ErrorResponse))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError,  Type = typeof(ErrorResponse))]
        public async Task<IActionResult> RegistrarVenda([FromBody] RegistrarVendaRequest request)
        {
            if(!ModelState.IsValid)
            {
                _logger.LogInformation("Modelo inválido", ModelState);
                return BadRequest(ModelState);
            }
             
            return Ok(await _useCaseResgistrarVenda.Execute(request));
        }

        /// <summary>
        /// Buscar Venda por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:Guid}")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(VendaResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ErrorResponse))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ErrorResponse))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ErrorResponse))]
        public async Task<IActionResult> BuscarVendaPorId(Guid id)
        {
     
                var response = await _useCaseBuscarVendaPorId.Execute(id);

                if (response == null)
                    return NotFound();

                return Ok(response);
        }

        /// <summary>
        /// Listar Vendas
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(List<VendaResponse>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ErrorResponse))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ErrorResponse))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ErrorResponse))]
        public async Task<IActionResult> ListarVendas()
        {
            var response = await _useCaseListarVendas.Execute();

            if (response == null)
                return NotFound();

            return Ok(response);

        }

        /// <summary>
        /// Atualizar Status de Venda
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(VendaResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ErrorResponse))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ErrorResponse))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ErrorResponse))]
        public async Task<IActionResult> AtualizarStatusVenda([FromRoute] Guid id, [FromBody] AtualizarStatusVendaRequest request)
        {
            var response = await _useCaseAtualizarStatusVenda.Execute(id, request);

            if (response == null)
                return NotFound();

            return Ok(response);
        }

    }

    
}
