﻿using AutoMapper;
using Pottencial.Payment.Borders.Dtos;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Api.Configurations
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<RegistrarVendaRequest, Venda>().ReverseMap();
            CreateMap<VendaResponse, Venda>().ReverseMap();

            CreateMap<VendedorDTO, Vendedor>().ReverseMap();

            CreateMap<ItemDTO, Item>().ReverseMap();


        }
    }
}
