﻿using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Repositories;
using Pottencial.Paymento.Infrastructure.UnitOfWorks;

namespace Pottencial.Payment.Api.Configurations
{
    public static class RepositoriesConfig
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services) => services
          .AddTransient<IUnitOfWork, UnitOfWork>()
          .AddTransient<IVendasRepository, VendasRepository>()
          .AddTransient<IItemRepository, ItemRepository>()
          .AddTransient<IVendedorRepository, VendedorRepository>();
    }
}
