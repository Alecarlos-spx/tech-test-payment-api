﻿using Microsoft.Extensions.DependencyInjection;
using Pottencial.Payment.Borders.UseCases;
using Pottencial.Payment.UseCases;

namespace Pottencial.Payment.Api.Configurations
{
    public static class UseCaseConfig
    {
        public static IServiceCollection AddUseCases(this IServiceCollection services) => services
            .AddTransient<IUseCaseResgistrarVenda, UseCaseRegistrarVenda>()
            .AddTransient<IUseCaseBuscarVendaPorId, UseCaseBuscarVendaPorId>()
            .AddTransient<IUseCaseListarVendas, UseCaseListarVendas>()
            .AddTransient<IUseCaseAtualizarStatusVenda, UseCaseAtualizarStatusVenda>();
    }
}
