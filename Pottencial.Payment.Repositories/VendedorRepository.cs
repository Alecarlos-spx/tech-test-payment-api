﻿using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Paymento.Infrastructure.Context;

namespace Pottencial.Payment.Repositories
{
    public class VendedorRepository : IVendedorRepository
    {
        private readonly SalesContext _context;

        public VendedorRepository(SalesContext context)
        {
            _context = context;
        }

        public async Task<Vendedor> AddSeller(Vendedor request)
        {
            _context.Vendedores.Add(request);

            _context.SaveChanges();

            return request;
        }

        public async Task<Vendedor> GetByIdSeller(Guid id)
        {
            return _context.Vendedores.Where(i => i.Id == id).FirstOrDefault();
        }
    }
}
