﻿using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Paymento.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Payment.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private readonly SalesContext _context;

        public ItemRepository(SalesContext context)
        {
            _context = context;
        }

        public async Task<Item> AddItem(Item request)
        {
            _context.Itens.Add(request);
            _context.SaveChanges();

            return request;
        }

        public async Task<List<Item>> GetItemByIdSale(Guid id)
        {
            var listaItens =  _context.Itens.Where(i => i.VendaId == id).ToList();
            return listaItens;
        }

        public Task<Item> GetByIdItem(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
