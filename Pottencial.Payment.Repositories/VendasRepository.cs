﻿using Pottencial.Payment.Borders.Repositories;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Paymento.Infrastructure.Context;

namespace Pottencial.Payment.Repositories
{
    public class VendasRepository : IVendasRepository
    {
        private readonly SalesContext _context;
        private readonly IUnitOfWork _unitOfWork;

        public VendasRepository(SalesContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        public async Task<Venda> AddSale(Venda request)
        {
            _context.Vendas.Add(request);
            await _unitOfWork.CommitAsync();

            return request;
        }

        public async Task<List<Venda>> GetAllSale()
        {
            return _context.Vendas.ToList();
        }

        public async Task<Venda> GetByIdSale(Guid id)
        {
            
            return _context.Vendas.Where(v => v.Id == id).FirstOrDefault();

        }

        public async Task<bool> UpdateStatus(Venda request)
        {
            
            if (request != null)
            {
                
                //_context.Entry(tarefa).CurrentValues.SetValues(request);
                _context.Vendas.Update(request);
                await _unitOfWork.CommitAsync();
                
                return true;
            }

            return false;
        }
    }
}
