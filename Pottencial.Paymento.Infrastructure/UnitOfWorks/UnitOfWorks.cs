﻿using Pottencial.Payment.Borders.Repositories;
using Pottencial.Paymento.Infrastructure.Context;

namespace Pottencial.Paymento.Infrastructure.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SalesContext _context;

        public UnitOfWork(SalesContext context)
        {
            _context = context;
        }

        public async Task<bool> CommitAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
