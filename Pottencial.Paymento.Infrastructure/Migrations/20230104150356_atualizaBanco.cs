﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Pottencial.Paymento.Infrastructure.Migrations
{
    public partial class atualizaBanco : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens");

            migrationBuilder.AddColumn<Guid>(
                name: "IdVendedor",
                table: "Vendas",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<Guid>(
                name: "VendaId",
                table: "Itens",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens",
                column: "VendaId",
                principalTable: "Vendas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens");

            migrationBuilder.DropColumn(
                name: "IdVendedor",
                table: "Vendas");

            migrationBuilder.AlterColumn<Guid>(
                name: "VendaId",
                table: "Itens",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens",
                column: "VendaId",
                principalTable: "Vendas",
                principalColumn: "Id");
        }
    }
}
