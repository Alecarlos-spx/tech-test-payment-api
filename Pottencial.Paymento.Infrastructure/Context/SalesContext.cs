﻿using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Paymento.Infrastructure.Context
{
    public class SalesContext : DbContext
    {
        public SalesContext(DbContextOptions<SalesContext> options) : base(options)  {}
        public DbSet<Item> Itens { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vendedor>()
               .HasKey(x => x.Id);

            modelBuilder.Entity<Venda>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Item>()
                .HasKey(x => x.Id);
        }
    }
}
